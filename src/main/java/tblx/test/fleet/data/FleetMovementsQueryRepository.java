package tblx.test.fleet.data;

import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface FleetMovementsQueryRepository {

    List<String> listOperatorsInInterval(Date startTime, Date endTime);

    List<Integer> listOperatorVehicles(String operator, Date startTime, Date endTime);

    List<Integer> listOperatorVehiclesAtStop(String operator, Date startTime, Date endTime);

    List<FleetMovements> listVehicleTelemetries(int vehicle, Date startTime, Date endTime);
}
