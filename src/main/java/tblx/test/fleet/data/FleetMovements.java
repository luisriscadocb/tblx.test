package tblx.test.fleet.data;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@ToString
@Document(collection = "fleetMovements")
public class FleetMovements {

    @Id
    private String entryId;

    private Instant timestamp;

    private String lineId;

    private Integer direction;

    private String journeyPatternID;

    private String timeFrame;

    private Integer vehicleJourneyID;

    private String operator;

    private Boolean congestion;

    private Double lon;

    private Double lat;

    private Integer delay;

    private Integer blockId;

    private Integer vehicleID;

    private Integer stopID;

    private Boolean atStop;

}
