package tblx.test.fleet.data;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class FleetMovementsQueryRepositoryImpl implements FleetMovementsQueryRepository {

    private final MongoTemplate mongoTemplate;

    @Override
    public List<String> listOperatorsInInterval(Date startTime, Date endTime) {

        Query query = Query.query(Criteria.where("timestamp").gte(startTime)
                .lte(endTime));

        return mongoTemplate.findDistinct(query, "operator", FleetMovements.class, String.class);
    }

    @Override
    public List<Integer> listOperatorVehicles(String operator, Date startTime, Date endTime) {

        Query query = Query.query(Criteria.where("timestamp").gte(startTime)
                .lte(endTime).and("operator").is(operator));

        return mongoTemplate.findDistinct(query, "vehicleID", FleetMovements.class, Integer.class);
    }

    @Override
    public List<Integer> listOperatorVehiclesAtStop(String operator, Date startTime, Date endTime) {
        Query query = Query.query(Criteria.where("timestamp").gte(startTime)
                .lte(endTime).and("operator").is(operator).and("atStop").is(true));

        return mongoTemplate.findDistinct(query, "vehicleID", FleetMovements.class, Integer.class);
    }

    @Override
    public List<FleetMovements> listVehicleTelemetries(int vehicle, Date startTime, Date endTime) {
        Query query = Query.query(Criteria.where("timestamp").gte(startTime)
                .lte(endTime).and("vehicleID").is(vehicle))
                .with(Sort.by(Sort.Direction.DESC, "timestamp"));

        return mongoTemplate.find(query, FleetMovements.class);
    }
}
