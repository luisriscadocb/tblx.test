package tblx.test.fleet.data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FleetMovementsRepository extends MongoRepository<FleetMovements, String> {


}
