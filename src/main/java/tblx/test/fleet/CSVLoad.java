package tblx.test.fleet;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tblx.test.fleet.service.csv.CSVLoadData;
import tblx.test.fleet.service.csv.CSVLoader;

import java.time.Duration;
import java.time.Instant;

@RestController
@RequestMapping("/csv")
@RequiredArgsConstructor
@Slf4j
public class CSVLoad {
    private final CSVLoader csvLoader;

    @PostMapping(value = "/")
    @ResponseBody
    @ApiOperation("Upload data to database through csv file")
    private ResponseEntity<CSVLoadData> loadCSVData(@ApiParam(value = "CSV file to load to database", required = true)
                                                    @RequestParam(value = "file", required = true) MultipartFile file) {
        final Instant start = Instant.now();
        CSVLoadData data = csvLoader.load(file);
        log.info("POST /csv/ took {} ms", Duration.between(start, Instant.now()).toMillis());
        return ResponseEntity.ok(data);
    }
}
