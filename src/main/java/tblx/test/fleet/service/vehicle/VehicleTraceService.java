package tblx.test.fleet.service.vehicle;

import java.util.Date;
import java.util.List;

public interface VehicleTraceService {

    List<VehicleTrace> trace(int vehicleId, Date startTime, Date endTime);
}
