package tblx.test.fleet.service.vehicle;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;

@Data
@EqualsAndHashCode
public class VehicleTraceDTO implements VehicleTrace {
    private Double lon;

    private Double lat;

    private int vehicleID;

    private Instant timestamp;
}
