package tblx.test.fleet.service.vehicle;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tblx.test.fleet.data.FleetMovements;
import tblx.test.fleet.data.FleetMovementsQueryRepository;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class VehicleTraceServiceImpl implements VehicleTraceService {
    private final FleetMovementsQueryRepository fleetMovementsQueryRepository;

    @Override
    public List<VehicleTrace> trace(int vehicleId, Date startTime, Date endTime) {

        List<FleetMovements> telemetries = fleetMovementsQueryRepository.listVehicleTelemetries(vehicleId, startTime, endTime);

        return telemetries.stream().map(this::map).collect(Collectors.toList());
    }


    private VehicleTraceDTO map(FleetMovements fleetMovements) {
        VehicleTraceDTO vehicleTraceDTO = new VehicleTraceDTO();
        vehicleTraceDTO.setLat(fleetMovements.getLat());
        vehicleTraceDTO.setLon(fleetMovements.getLon());
        vehicleTraceDTO.setTimestamp(fleetMovements.getTimestamp());
        vehicleTraceDTO.setVehicleID(fleetMovements.getVehicleID());
        return vehicleTraceDTO;
    }
}
