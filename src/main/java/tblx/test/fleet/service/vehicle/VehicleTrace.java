package tblx.test.fleet.service.vehicle;

import java.time.Instant;

public interface VehicleTrace {

    Double getLon();

    Double getLat();

    int getVehicleID();

    Instant getTimestamp();
}
