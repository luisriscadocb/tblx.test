package tblx.test.fleet.service.csv;

import com.opencsv.CSVReader;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tblx.test.fleet.data.FleetMovements;
import tblx.test.fleet.data.FleetMovementsRepository;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@Service
@Slf4j
@RequiredArgsConstructor
class CSVLoaderImpl implements CSVLoader {

    private final FleetMovementsRepository fleetMovementsRepository;

    public CSVLoadData load(MultipartFile csv) {
        long total = 0;
        long success = 0;
        long error = 0;
        List<FleetMovements> fleetTelemetries = new LinkedList<>();
        try (InputStream in = csv.getInputStream();
             Reader reader = new InputStreamReader(in);
             CSVReader csvReader = new CSVReader(reader)) {

            String[] line;
            while ((line = csvReader.readNext()) != null) {
                ++total;
                try {
                    processCSVData(line, fleetTelemetries::add);
                    ++success;
                } catch (Exception e) {
                    log.error("Error processing CSV line " + total, e);
                    ++error;
                }
            }

        } catch (Exception e) {
            log.error("Error reading CSV file " + csv.getOriginalFilename(), e);
            return new CSVLoadData("Error processing CSV data");
        }

        //batch insert 10 times faster
        fleetMovementsRepository.insert(fleetTelemetries);

        final CSVLoadData csvLoadData = new CSVLoadData();
        csvLoadData.setTotalEntries(total);
        csvLoadData.setSuccessEntries(success);
        csvLoadData.setErrorEntries(error);
        return csvLoadData;
    }


    private void processCSVData(String[] rawData, Consumer<FleetMovements> store) {
        final FleetMovements data = new FleetMovements();
        data.setTimestamp(getInstantFromMicros(Long.valueOf(rawData[0])));
        data.setLineId(clearString(rawData[1]));
        data.setDirection(intergerVal(rawData[2]));
        data.setJourneyPatternID(clearString(rawData[3]));
        data.setTimeFrame(clearString(rawData[4]));
        data.setVehicleJourneyID(intergerVal(rawData[5]));
        data.setOperator(clearString(rawData[6]));
        data.setCongestion(booleanVal(rawData[7]));
        data.setLon(doubleVal(rawData[8]));
        data.setLat(doubleVal(rawData[9]));
        data.setDelay(intergerVal(rawData[10]));
        data.setBlockId(intergerVal(rawData[11]));
        data.setVehicleID(intergerVal(rawData[12]));
        data.setStopID(intergerVal(rawData[13]));
        data.setAtStop(booleanVal(rawData[14]));
        store.accept(data);
    }

    private Boolean booleanVal(String rawDatum) {
        if ("0".equals(rawDatum)) {
            return false;
        } else if ("1".equals(rawDatum)) {
            return true;
        }

        return null;
    }

    private String clearString(String val) {

        return Optional.ofNullable(val)
                .map(String::trim)
                .map(v -> {
                    if (v.isEmpty() || "null".equalsIgnoreCase(v)) {
                        return null;
                    }
                    return v;

                }).orElse(null);
    }

    private Double doubleVal(String val) {
        val = clearString(val);
        if (val == null)
            return null;
        return Double.valueOf(val);
    }

    private Integer intergerVal(String val) {
        val = clearString(val);
        if (val == null)
            return null;
        return Integer.valueOf(val);
    }

    private Instant getInstantFromMicros(Long microsSinceEpoch) {
        return Instant.ofEpochSecond(TimeUnit.MICROSECONDS.toSeconds(microsSinceEpoch), TimeUnit.MICROSECONDS.toNanos(Math.floorMod(microsSinceEpoch, TimeUnit.SECONDS.toMicros(1))));
    }
}
