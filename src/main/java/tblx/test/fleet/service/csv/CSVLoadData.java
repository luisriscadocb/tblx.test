package tblx.test.fleet.service.csv;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class CSVLoadData {
    public CSVLoadData(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    private long totalEntries;

    private long successEntries;

    private long errorEntries;

    private String errorMessage;
}
