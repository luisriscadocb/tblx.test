package tblx.test.fleet.service.csv;

import org.springframework.web.multipart.MultipartFile;

public interface CSVLoader {

    CSVLoadData load(MultipartFile csv);
}
