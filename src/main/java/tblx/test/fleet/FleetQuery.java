package tblx.test.fleet;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tblx.test.fleet.data.FleetMovementsQueryRepository;
import tblx.test.fleet.service.vehicle.VehicleTrace;
import tblx.test.fleet.service.vehicle.VehicleTraceService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/query")
@RequiredArgsConstructor
@Slf4j
public class FleetQuery {

    static final String TS_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    private final FleetMovementsQueryRepository fleetMovementsQueryRepository;

    private final VehicleTraceService vehicleTraceService;

    @GetMapping(value = "/runningOperators")
    @ResponseBody
    @ApiOperation("List running operators within a time frame (start-time, end-time)")
    public ResponseEntity<List<String>> runningOperators(@ApiParam(value = "Start time for the search interval (yyyy-MM-ddTHH:mm:ss)", required = true,
            example = "2012-11-14T12:00:00")
                                                         @RequestParam("start-time")
                                                         @DateTimeFormat(pattern = TS_PATTERN) @Valid @NotNull
                                                                 Date startTime,
                                                         @ApiParam(value = "End time for the search interval (yyyy-MM-ddTHH:mm:ss)", required = true,
                                                                 example = "2012-11-14T12:00:00")
                                                         @RequestParam("end-time") @DateTimeFormat(pattern = TS_PATTERN)
                                                         @Valid @NotNull
                                                                 Date endTime) {
        final Instant start = Instant.now();
        log.info("GET /query/runningOperators start-time: {}, end-time: {}", startTime, endTime);

        //query running operators - return operator ids
        List<String> data = fleetMovementsQueryRepository.listOperatorsInInterval(startTime, endTime);

        log.info("GET /query/runningOperators took {} ms", Duration.between(start, Instant.now()).toMillis());

        return ResponseEntity.ok(data);
    }

    @GetMapping(value = "/operator/{operator}/vehicles")
    @ResponseBody
    @ApiOperation("List running vehicles from an operator within a time frame (start-time, end-time)")
    public ResponseEntity<List<Integer>> operatorVehicles(@ApiParam(value = "Start time for the search interval (yyyy-MM-ddTHH:mm:ss)", required = true,
            example = "2012-11-14T12:00:00")
                                                          @RequestParam("start-time") @DateTimeFormat(pattern = TS_PATTERN)
                                                          @Valid @NotNull Date startTime,
                                                          @ApiParam(value = "End time for the search interval (yyyy-MM-ddTHH:mm:ss)", required = true,
                                                                  example = "2012-11-14T12:00:00")
                                                          @RequestParam("end-time") @DateTimeFormat(pattern = TS_PATTERN)
                                                          @Valid @NotNull Date endTime,
                                                          @ApiParam(value = "Operator id", required = true, example = "CL")
                                                          @PathVariable("operator") @Valid @NotNull String operator) {
        final Instant start = Instant.now();
        log.info("GET /query/operator/{}/vehicles start-time: {}, end-time: {}", operator, startTime, endTime);

        // query operator vehicles / return vehicle ids
        List<Integer> data = fleetMovementsQueryRepository.listOperatorVehicles(operator, startTime, endTime);

        log.info("GET /query/operator/{}/vehicles took {} ms", operator, Duration.between(start, Instant.now()).toMillis());

        return ResponseEntity.ok(data);
    }

    @GetMapping(value = "/operator/{operator}/atStopVehicles")
    @ResponseBody
    @ApiOperation("List vehicles at a stop from an operator within a time frame (start-time, end-time)")
    public ResponseEntity<List<Integer>> operatorAtStopVehicles(@ApiParam(value = "Start time for the search interval (yyyy-MM-ddTHH:mm:ss)", required = true,
            example = "2012-11-14T12:00:00")
                                                                @RequestParam("start-time") @DateTimeFormat(pattern = TS_PATTERN)
                                                                @Valid @NotNull Date startTime,
                                                                @ApiParam(value = "End time for the search interval (yyyy-MM-ddTHH:mm:ss)", required = true,
                                                                        example = "2012-11-14T12:00:00")
                                                                @RequestParam("end-time") @DateTimeFormat(pattern = TS_PATTERN)
                                                                @Valid @NotNull Date endTime,
                                                                @ApiParam(value = "Operator id", required = true, example = "CL")
                                                                @PathVariable("operator") @Valid @NotNull String operator) {
        final Instant start = Instant.now();
        log.info("GET /query/operator/{}/atStopVehicles start-time: {}, end-time: {}", operator, startTime, endTime);

        //TODO query fleet at stop vehicles / return vehicle ids
        List<Integer> data = fleetMovementsQueryRepository.listOperatorVehiclesAtStop(operator, startTime, endTime);

        log.info("GET /query/operator/{}/atStopVehicles took {} ms", operator, Duration.between(start, Instant.now()).toMillis());

        return ResponseEntity.ok(data);
    }

    @GetMapping(value = "/vehicle/{vehicle}/trace")
    @ResponseBody
    @ApiOperation("List the list of location traces of a vehicle id within a time frame (start-time, end-time) ordered by timestamp")
    public ResponseEntity<List<VehicleTrace>> traceVehicle(@ApiParam(value = "Start time for the search interval (yyyy-MM-ddTHH:mm:ss)", required = true,
            example = "2012-11-14T12:00:00")
                                                           @RequestParam("start-time") @DateTimeFormat(pattern = TS_PATTERN)
                                                           @Valid @NotNull Date startTime,
                                                           @ApiParam(value = "End time for the search interval (yyyy-MM-ddTHH:mm:ss)", required = true,
                                                                   example = "2012-11-14T12:00:00")
                                                           @RequestParam("end-time") @DateTimeFormat(pattern = TS_PATTERN)
                                                           @Valid @NotNull Date endTime,
                                                           @ApiParam(value = "Vehicle Id to trace", required = true)
                                                           @PathVariable("vehicle") int vehicle) {
        final Instant start = Instant.now();
        log.info("GET /query/vehicle/{}/trace start-time: {}, end-time: {}", vehicle, startTime, endTime);

        //query vehicleTrace / return GPS entries order by timestamp
        List<VehicleTrace> data = vehicleTraceService.trace(vehicle, startTime, endTime);

        log.info("GET /query/vehicle/{}/trace took {} ms", vehicle, Duration.between(start, Instant.now()).toMillis());

        return ResponseEntity.ok(data);
    }
}
