package tblx.test.fleet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@SpringBootApplication
public class FleetMain {

    public static void main(String[] args) {
        SpringApplication.run(FleetMain.class, args);
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder().title("Fleet@tblx")
                        .description("Load and query fleet movement data")
                        .build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("tblx.test.fleet"))
                .paths(PathSelectors.any())
                .build();
    }
}
