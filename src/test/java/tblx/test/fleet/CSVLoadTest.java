package tblx.test.fleet;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import tblx.test.fleet.service.csv.CSVLoadData;
import tblx.test.fleet.service.csv.CSVLoader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class CSVLoadTest {

    @MockBean
    private CSVLoader csvLoader;

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void loadCSVData() throws Exception {
        CSVLoadData csvLoadData = new CSVLoadData();
        csvLoadData.setErrorEntries(1);
        csvLoadData.setSuccessEntries(1);
        csvLoadData.setTotalEntries(2);
        when(csvLoader.load(any())).thenReturn(csvLoadData);

        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.csv",
                MediaType.TEXT_PLAIN_VALUE,
                "mock".getBytes()
        );

        MvcResult result = this.mockMvc.perform(multipart("/csv/")
                .file(file))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        CSVLoadData dataReturned = new ObjectMapper().readValue(result.getResponse().getContentAsByteArray(), CSVLoadData.class);

        assertEquals(csvLoadData, dataReturned);
    }

}