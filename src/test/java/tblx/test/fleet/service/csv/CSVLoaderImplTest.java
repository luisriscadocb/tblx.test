package tblx.test.fleet.service.csv;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;
import tblx.test.fleet.data.FleetMovements;
import tblx.test.fleet.data.FleetMovementsRepository;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CSVLoaderImplTest {
    @Mock
    private FleetMovementsRepository fleetMovementsRepository;

    @InjectMocks
    private CSVLoaderImpl csvLoader;

    @Mock
    private MultipartFile file;

    @Captor
    private ArgumentCaptor<List<FleetMovements>> captureInsert;

    @Test
    void load() throws IOException {
        when(file.getInputStream())
                .thenAnswer(invocationOnMock -> this.getClass().getResourceAsStream("test.csv"));

        CSVLoadData load = csvLoader.load(file);

        verify(fleetMovementsRepository).insert(captureInsert.capture());

        assertNotNull(load);
        assertNotNull(captureInsert.getValue());
        assertEquals(223, load.getTotalEntries());
        assertEquals(load.getTotalEntries(), load.getSuccessEntries());
        assertEquals(0, load.getErrorEntries());
        assertNull(load.getErrorMessage());
        assertEquals(captureInsert.getValue().size(), load.getSuccessEntries());

        when(file.getInputStream()).thenReturn(null);

        load = csvLoader.load(file);

        assertEquals("Error processing CSV data", load.getErrorMessage());

    }
}