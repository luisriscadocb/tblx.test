package tblx.test.fleet.service.vehicle;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tblx.test.fleet.data.FleetMovements;
import tblx.test.fleet.data.FleetMovementsQueryRepository;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VehicleTraceServiceImplTest {

    @Mock
    private FleetMovementsQueryRepository fleetMovementsQueryRepository;

    @InjectMocks
    private VehicleTraceServiceImpl vehicleTraceService;

    @Test
    void trace() {
        int vehicle = 5;
        Date startTime = new Date();
        Date endTime = new Date();

        FleetMovements fleetMovements1 = new FleetMovements();
        fleetMovements1.setVehicleID(vehicle);
        fleetMovements1.setLat(1d);
        fleetMovements1.setLon(2d);
        fleetMovements1.setTimestamp(Instant.now());


        FleetMovements fleetMovements2 = new FleetMovements();
        fleetMovements2.setVehicleID(vehicle);
        fleetMovements2.setLat(3d);
        fleetMovements2.setLon(4d);
        fleetMovements2.setTimestamp(Instant.now());

        when(fleetMovementsQueryRepository.listVehicleTelemetries(vehicle, startTime, endTime))
                .thenReturn(Arrays.asList(fleetMovements1, fleetMovements2));

        List<VehicleTrace> trace = vehicleTraceService.trace(vehicle, startTime, endTime);

        assertEquals(2, trace.size());

        assertTrue(trace.stream().anyMatch(t ->
                Objects.equals(t.getVehicleID(), fleetMovements1.getVehicleID()) &&
                        Objects.equals(t.getLat(), fleetMovements1.getLat()) &&
                        Objects.equals(t.getLon(), fleetMovements1.getLon()) &&
                        Objects.equals(t.getTimestamp(), fleetMovements1.getTimestamp())));

        assertTrue(trace.stream().anyMatch(t ->
                Objects.equals(t.getVehicleID(), fleetMovements2.getVehicleID()) &&
                        Objects.equals(t.getLat(), fleetMovements2.getLat()) &&
                        Objects.equals(t.getLon(), fleetMovements2.getLon()) &&
                        Objects.equals(t.getTimestamp(), fleetMovements2.getTimestamp())));
    }
}