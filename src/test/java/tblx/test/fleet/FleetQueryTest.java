package tblx.test.fleet;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import tblx.test.fleet.data.FleetMovementsQueryRepository;
import tblx.test.fleet.service.csv.CSVLoadData;
import tblx.test.fleet.service.vehicle.VehicleTrace;
import tblx.test.fleet.service.vehicle.VehicleTraceDTO;
import tblx.test.fleet.service.vehicle.VehicleTraceService;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class FleetQueryTest {
    @MockBean
    private FleetMovementsQueryRepository fleetMovementsQueryRepository;

    @MockBean
    private VehicleTraceService vehicleTraceService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void runningOperators() throws Exception {
        mockMvc.perform(get("/query/runningOperators"))
                .andExpect(status().is(400));
        String startTime = "2020-10-12T13:23:23";
        String endTime = "2020-10-12T13:23:24";
        List<String> operators = Collections.singletonList("op");

        when(fleetMovementsQueryRepository.listOperatorsInInterval(stringToDate(startTime),
                stringToDate(endTime)))
                .thenReturn(operators);

        MvcResult result = mockMvc.perform(get("/query/runningOperators")
                .param("start-time", startTime)
                .param("end-time", endTime))
                .andExpect(status().isOk())
                .andReturn();

        List<String> dataReturned = new ObjectMapper().readValue(result.getResponse().getContentAsByteArray(),
                new TypeReference<List<String>>() {
                });

        assertEquals(operators, dataReturned);
    }

    @Test
    void operatorVehicles() throws Exception {
        String operator = "op";
        mockMvc.perform(get("/query/operator/{operator}/vehicles", operator))
                .andExpect(status().is(400));
        String startTime = "2020-10-12T13:23:23";
        String endTime = "2020-10-12T13:23:24";

        List<Integer> vehicles = Arrays.asList(1001, 1002, 1003);

        when(fleetMovementsQueryRepository.listOperatorVehicles(operator, stringToDate(startTime),
                stringToDate(endTime)))
                .thenReturn(vehicles);

        MvcResult result = mockMvc.perform(get("/query/operator/{operator}/vehicles", operator)
                .param("start-time", startTime)
                .param("end-time", endTime))
                .andExpect(status().isOk())
                .andReturn();

        List<Integer> dataReturned = new ObjectMapper().readValue(result.getResponse().getContentAsByteArray(),
                new TypeReference<List<Integer>>() {
                });

        assertEquals(vehicles, dataReturned);
    }

    @Test
    void operatorAtStopVehicles() throws Exception {
        String operator = "op";
        mockMvc.perform(get("/query/operator/{operator}/atStopVehicles", operator))
                .andExpect(status().is(400));
        String startTime = "2020-10-12T13:23:23";
        String endTime = "2020-10-12T13:23:24";

        List<Integer> vehicles = Arrays.asList(1001, 1002, 1003);

        when(fleetMovementsQueryRepository.listOperatorVehiclesAtStop(operator, stringToDate(startTime),
                stringToDate(endTime)))
                .thenReturn(vehicles);

        MvcResult result = mockMvc.perform(get("/query/operator/{operator}/atStopVehicles", operator)
                .param("start-time", startTime)
                .param("end-time", endTime))
                .andExpect(status().isOk())
                .andReturn();

        List<Integer> dataReturned = new ObjectMapper().readValue(result.getResponse().getContentAsByteArray(),
                new TypeReference<List<Integer>>() {
                });

        assertEquals(vehicles, dataReturned);
    }

    @Test
    void traceVehicle() throws Exception {
        int vehicle = 1234;
        mockMvc.perform(get("/query/vehicle/{vehicle}/trace", vehicle))
                .andExpect(status().is(400));
        String startTime = "2020-10-12T13:23:23";
        String endTime = "2020-10-12T13:23:24";
        VehicleTraceDTO vehicleTraceDTO = new VehicleTraceDTO();
        vehicleTraceDTO.setVehicleID(vehicle);
        vehicleTraceDTO.setTimestamp(Instant.now());
        vehicleTraceDTO.setLon(-30.234234D);
        vehicleTraceDTO.setLat(10.234234D);
        List<VehicleTrace> trace = Collections.singletonList(vehicleTraceDTO);

        when(vehicleTraceService.trace(vehicle, stringToDate(startTime),
                stringToDate(endTime)))
                .thenReturn(trace);

        MvcResult result = mockMvc.perform(get("/query/vehicle/{vehicle}/trace", vehicle)
                .param("start-time", startTime)
                .param("end-time", endTime))
                .andExpect(status().isOk())
                .andReturn();
        ObjectMapper mapper = new ObjectMapper();
        JavaTimeModule module = new JavaTimeModule();
        mapper.registerModule(module);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        List<VehicleTraceDTO> dataReturned = mapper.readValue(result.getResponse().getContentAsByteArray(),
                new TypeReference<List<VehicleTraceDTO>>() {
                });

        assertEquals(trace, dataReturned);
    }


    private Date stringToDate(String date) throws ParseException {
        return new SimpleDateFormat(FleetQuery.TS_PATTERN).parse(date);
    }
}