package tblx.test.fleet.data;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FleetMovementsQueryRepositoryImplTest {

    @Mock
    private MongoTemplate mongoTemplate;

    @InjectMocks
    private FleetMovementsQueryRepositoryImpl fleetMovementsQueryRepository;

    @Test
    void listOperatorsInInterval() {
        List<String> data = Collections.singletonList("test");
        when(mongoTemplate.findDistinct(any(Query.class), eq("operator"),
                eq(FleetMovements.class), eq(String.class))).thenReturn(data);

        List<String> result = fleetMovementsQueryRepository.listOperatorsInInterval(new Date(), new Date());

        assertEquals(data, result);
    }

    @Test
    void listOperatorVehicles() {
        String operator = "CT";
        List<Integer> data = Collections.singletonList(2);
        when(mongoTemplate.findDistinct(any(Query.class), eq("vehicleID"),
                eq(FleetMovements.class), eq(Integer.class))).thenReturn(data);

        List<Integer> result = fleetMovementsQueryRepository.listOperatorVehicles(operator, new Date(), new Date());

        assertEquals(data, result);
    }

    @Test
    void listOperatorVehiclesAtStop() {
        String operator = "CT";
        List<Integer> data = Collections.singletonList(2);
        when(mongoTemplate.findDistinct(any(Query.class), eq("vehicleID"),
                eq(FleetMovements.class), eq(Integer.class))).thenReturn(data);

        List<Integer> result = fleetMovementsQueryRepository.listOperatorVehiclesAtStop(operator, new Date(), new Date());

        assertEquals(data, result);
    }

    @Test
    void listVehicleTelemetries() {
        int vehicle = 2;

        List<FleetMovements> data = Collections.singletonList(new FleetMovements());
        when(mongoTemplate.find(any(Query.class), eq(FleetMovements.class)))
                .thenReturn(data);

        List<FleetMovements> result = fleetMovementsQueryRepository
                .listVehicleTelemetries(vehicle, new Date(), new Date());

        assertEquals(data, result);
    }
}